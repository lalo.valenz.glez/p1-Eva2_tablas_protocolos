//
//  ViewController.swift
//  Eva2_1_Tablas_Protocolos
//
//  Created by TEMPORAL2 on 05/10/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var cosas = ["Cosa 1","Cosa 2","Cosa 3","Cosa 4","Cosa 5","Cosa 6","Cosa 7","Cosa 8","Cosa 9",
                "Cosa 10 ","Cosa 11","Cosa 12","Cosa 13","Cosa 14","Cosa 15","Cosa 16","Cosa 17","Cosa 18",
                "Cosa 19","Cosa 20 ","Cosa 21","Cosa 22","Cosa 23","Cosa 24","Cosa 25","Cosa 26","Cosa 27",
                "Cosa 28","Cosa 29","Cosa 30"]
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        let celda = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        celda.textLabel?.text = cosas[indexPath.row]
        
        return celda
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cosa = cosas[indexPath.row]
        let alerta = UIAlertController(title: "Seleccionaste", message: "\(cosa)", preferredStyle: .Alert)
        let action = UIAlertAction(title: "ok", style: .Destructive, handler: nil)
        alerta.addAction(action)
        presentViewController(alerta, animated: true, completion: nil)

    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cosas.count
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}

